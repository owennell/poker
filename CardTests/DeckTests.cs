﻿using System;
using System.Linq;
using Poker;
using NUnit.Framework;

namespace CardTests
{
    [TestFixture]
    public class DeckTests
    {
        [Test]
        public void ShouldCreateANewDeckOfCards52UniqueCards()
        {
            var deck = new Deck();

            AssertThatEachCardIsUnique(deck);
            Assert.That(deck.Cards.Count, Is.EqualTo(52));
        }

        private static void AssertThatEachCardIsUnique(Deck deck)
        {
            foreach (var card in deck.Cards)
            {
                var instancesOfCard = 0;
                foreach (var playingCard in deck.Cards)
                {
                    if (card.Display == playingCard.Display)
                        instancesOfCard++;
                }

                Assert.That(instancesOfCard, Is.EqualTo(1), 
                    string.Format("Deck contains Duplicate Cards. There are {0} instances of {1} ", instancesOfCard, card.Display));
            }
        }

        [Test]
        public void ShouldShuffleDeck()
        {
            var deck = new Deck();
            var originalOrderOfDeck = deck.Cards;

            deck.Shuffle();

            var cardsNoLongerInTheSameOrder = originalOrderOfDeck.SequenceEqual(deck.Cards);
            
            Assert.That(cardsNoLongerInTheSameOrder, Is.False);

            foreach (var card in deck.Cards)
            {
                Console.WriteLine(card.Display);    
            }

            AssertThatEachCardIsUnique(deck);
        }
    }
}
