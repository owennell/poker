﻿using Poker;
using NUnit.Framework;

[TestFixture]
public class PlayingCardTests
{
    [TestCase(Suit.Diamonds, FaceValue.Two)]
    [TestCase(Suit.Diamonds, FaceValue.Three)]
    [TestCase(Suit.Diamonds, FaceValue.Four)]
    [TestCase(Suit.Diamonds, FaceValue.Five)]
    [TestCase(Suit.Diamonds, FaceValue.Six)]
    [TestCase(Suit.Diamonds, FaceValue.Seven)]
    [TestCase(Suit.Diamonds, FaceValue.Eight)]
    [TestCase(Suit.Diamonds, FaceValue.Nine)]
    [TestCase(Suit.Diamonds, FaceValue.Ten)]
    [TestCase(Suit.Diamonds, FaceValue.Jack)]
    [TestCase(Suit.Diamonds, FaceValue.Queen)]
    [TestCase(Suit.Diamonds, FaceValue.King)]
    [TestCase(Suit.Diamonds, FaceValue.Ace)]
    [TestCase(Suit.Hearts, FaceValue.Ace)]
    [TestCase(Suit.Spades, FaceValue.Ace)]
    [TestCase(Suit.Clubs, FaceValue.Ace)]
    public void ShouldCreateANewPlayingCardInstance(Suit suit, FaceValue faceValue)
    {
        var card = new PlayingCard(suit, faceValue);

        Assert.That(card.Suit, Is.EqualTo(suit));
        Assert.That(card.FaceValue, Is.EqualTo(faceValue));
    }

    [TestCase(Suit.Diamonds, FaceValue.Two, "D2")]
    [TestCase(Suit.Diamonds, FaceValue.Three, "D3")]
    [TestCase(Suit.Diamonds, FaceValue.Four, "D4")]
    [TestCase(Suit.Diamonds, FaceValue.Five, "D5")]
    [TestCase(Suit.Diamonds, FaceValue.Six, "D6")]
    [TestCase(Suit.Diamonds, FaceValue.Seven, "D7")]
    [TestCase(Suit.Diamonds, FaceValue.Eight, "D8")]
    [TestCase(Suit.Diamonds, FaceValue.Nine, "D9")]
    [TestCase(Suit.Diamonds, FaceValue.Ten, "DT")]
    [TestCase(Suit.Diamonds, FaceValue.Jack, "DJ")]
    [TestCase(Suit.Diamonds, FaceValue.Queen, "DQ")]
    [TestCase(Suit.Diamonds, FaceValue.King, "DK")]
    [TestCase(Suit.Diamonds, FaceValue.Ace, "DA")]
    [TestCase(Suit.Hearts, FaceValue.Ace, "HA")]
    [TestCase(Suit.Spades, FaceValue.Ace, "SA")]
    [TestCase(Suit.Clubs, FaceValue.Ace, "CA")]
    public void ShouldReturnCorrectDisplayValueOfPlayingCard(Suit suit, FaceValue faceValue, string expectedDisplayCode)
    {
        var card = new PlayingCard(suit, faceValue);

        Assert.That(card.Display, Is.EqualTo(expectedDisplayCode));
    }
}
