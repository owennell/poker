﻿using System;

namespace Poker
{
    public enum Suit
    {
        Diamonds,
        Hearts,
        Clubs,
        Spades
    }

    public enum FaceValue
    {
        Two = 2,
        Three = 3,
        Four = 4,
        Five = 5,
        Six = 6,
        Seven = 7,
        Eight = 8,
        Nine = 9,
        Ten = 10,
        Jack = 11,
        Queen = 12,
        King = 13,
        Ace = 14
    }

    public class PlayingCard : IComparable
    {
        private readonly Suit _suit;
        private readonly FaceValue _faceValue;

        public PlayingCard(Suit suit, FaceValue faceValue)
        {
            _suit = suit;
            _faceValue = faceValue;
        }

        public Suit Suit { get { return _suit; }  }

        public FaceValue FaceValue
        {
            get { return _faceValue; }
        }

        public string Display
        {
            get
            {
                var suitCode = _suit.ToString().Substring(0, 1);
                var cardCode = _faceValue.ToString().Substring(0, 1);
                var valueOfCard = (int)_faceValue;
                if (valueOfCard <= 9)
                {
                    cardCode = valueOfCard.ToString();
                }
                
                return string.Concat(suitCode,cardCode);
            }
        }

        public int CompareTo(object cardToCompare)
        {
            var cardToCompareTo = (PlayingCard) cardToCompare;
            if (this.FaceValue > cardToCompareTo.FaceValue) return 1;
            if (this.FaceValue == cardToCompareTo.FaceValue) return 0;

            return -1;
        }
    }
}