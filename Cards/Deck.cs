using System;
using System.Collections.Generic;
using System.Linq;

namespace Poker
{
    public class Deck
    {
        private IList<PlayingCard> _playingCards;

        public Deck()
        {
            _playingCards = CreateNew52CardDeck();
        }

        private List<PlayingCard> CreateNew52CardDeck()
        {
            var deck = new List<PlayingCard>();
            var suits = Enum.GetValues(typeof(Suit)).Cast<Suit>();
            foreach (var suit in suits)
            {
                var faceValue = 2;
                while (faceValue <= (int)FaceValue.Ace)
                {
                    deck.Add(new PlayingCard(suit, (FaceValue)faceValue));
                    faceValue++;
                }
            }

            return deck;
        }
        
        public IList<PlayingCard> Cards { get { return _playingCards; } }

        public void Shuffle()
        {
            var deckToShuffle = _playingCards;
            var shuffledDeck = new List<PlayingCard>(deckToShuffle.Count);
            var seed = new Random(DateTime.Now.Millisecond);

            while (deckToShuffle.Count > 0)
            {
                var indexOfCard = seed.Next(0, deckToShuffle.Count);
                shuffledDeck.Add(deckToShuffle[indexOfCard]);
                deckToShuffle.RemoveAt(indexOfCard);
            }

            _playingCards = shuffledDeck;
        }
    }
}