﻿using System.Collections.Generic;

namespace Poker.Hands
{
    public class Straight : PokerHand, IPokerHand
    {
        public bool PossibleFromPlayingCards(PlayingCard c1, PlayingCard c2, PlayingCard c3, PlayingCard c4, PlayingCard c5)
        {
            var playingCards = new List<PlayingCard>() { c1, c2, c3, c4, c5 };
            playingCards.Sort();

            for (var x = 0; x < (playingCards.Count - 1); x++)
            {
                var currentPlayingCardValue = playingCards[x].FaceValue;
                var nextExpectedValue = (currentPlayingCardValue + 1);
                var nextPlayingCardValue = (playingCards[x + 1].FaceValue);

                if (nextPlayingCardValue != nextExpectedValue)
                    return false;
            }

            return true;
        }

        public override int Ranking
        {
            get { return 5; }
        }
    }
}