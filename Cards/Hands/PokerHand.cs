﻿using System;

namespace Poker.Hands
{
    public interface IPokerHand
    {
        bool PossibleFromPlayingCards(PlayingCard c1, PlayingCard c2, PlayingCard c3, PlayingCard c4, PlayingCard c5);
        int Ranking { get; }
    }

    public abstract class PokerHand : IComparable
    {
        public int CompareTo(object handToCompare)
        {
            var hand = (IPokerHand)handToCompare;

            if (this.Ranking < hand.Ranking) return 1;
            if (this.Ranking == hand.Ranking) return 0;
            return -1;
        }

        public virtual int Ranking
        {
            get { return -1; }
        }
    }
}
