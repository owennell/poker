﻿using System.Collections.Generic;

namespace Poker.Hands
{
    public class Flush : PokerHand, IPokerHand
    {
        public bool PossibleFromPlayingCards(PlayingCard c1, PlayingCard c2, PlayingCard c3, PlayingCard c4, PlayingCard c5)
        {
            var playingCards = new List<PlayingCard>() { c1, c2, c3, c4, c5 };

            foreach (var playingCard in playingCards)
            {
                if (playingCard.Suit != c1.Suit) return false;
            }

            return true;
        }

        public override int Ranking
        {
            get { return 6; }
        }
    }
}
