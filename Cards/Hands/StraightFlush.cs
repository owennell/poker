namespace Poker.Hands
{
    public class StraightFlush : PokerHand, IPokerHand
    {
        public bool PossibleFromPlayingCards(PlayingCard c1, PlayingCard c2, PlayingCard c3, PlayingCard c4, PlayingCard c5)
        {
            var straight = new Straight();
            var flush = new Flush();
            if (!straight.PossibleFromPlayingCards(c1, c2, c3, c4, c5)) return false;
            if (!flush.PossibleFromPlayingCards(c1, c2, c3, c4, c5)) return false;

            return true;
        }

        public override int Ranking
        {
            get { return 9; }
        }
    }
}