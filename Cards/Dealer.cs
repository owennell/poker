﻿using System;
using System.Collections.Generic;
using Poker.Hands;

namespace Poker
{
    public class Dealer
    {
        private static IEnumerable<IPokerHand> SetupWinningHandsInRankOrder()
        {
            var hands = new List<IPokerHand>(){
                new StraightFlush(),
                //new FourOfAKind(),
                //new FullHouse(),
                new Flush(),
                new Straight(),
                //new ThreeOfAKind(),
                //new TwoPairs(),
                //new OnePair(),
                //new HighPlayingCard()
            };

            return hands;
        }

        public IPokerHand DetermineBestHandFromAvailablePlayingCards(PlayingCard c1, PlayingCard c2, PlayingCard c3, PlayingCard c4, PlayingCard c5)
        {
            var hands = SetupWinningHandsInRankOrder();

            foreach (var pokerHand in hands)
            {
                if (pokerHand.PossibleFromPlayingCards(c1, c2, c3, c4, c5)) return pokerHand;
            }

            throw new Exception("No Hand Found");
        }

        public IPokerHand DetermineWinner(List<IPokerHand> hands)
        {
            hands.Sort();

            return hands[0];
        }
    }
}
